AWS RDS Aurora Postgres
=======================
This role is part of the [Mirabeau Cloud Framework](https://gitlab.com/mirabeau/cloud-framework/)

Create Aurora Postgres services with CloudFormation.

AWS resources that will be created are:
 * Aurora PostgreSQL service
 * SecurityGroup

Requirements
------------
Ansible version 2.5.4 or higher
Python 2.7.x

Required python modules:
 * boto
 * boto3
 * awscli

Dependencies
------------
 * aws-vpc-lambda-cfn-dbprovider
 * aws-iam
 * aws-vpc

Role Variables
--------------
### _General_
The following params should be available for Ansible during the rollout of this role:
```yaml
aws_region      : <aws region, eg: eu-west-1>
owner           : <owner, eg: mirabeau>
account_name    : <aws account name>
account_abbr    : <aws account generic environment>
environment_type: <environment>
environment_abbr: <environment abbriviation>
slicename       : <slicename>
```

Role Defaults
-------------
```yaml
create_changeset      : True
debug                 : False
cloudformation_tags   : {}
tag_prefix            : "mcf"

aws_rds_aurora_postgres_name : ""

aws_rds_aurora_postgres_params:
  stack_name      : "{{ slicename }}-data"

  create_changeset: "{{ create_changeset }}"
  debug           : "{{ debug }}"

  database:
    name         : "{{ aws_rds_aurora_postgres_name }}"  # Name of the db scheme created by RDS
    username     : "{{ aws_rds_aurora_postgres_name }}"  # DB scheme user, password will be auto generated and saved in SSM

  rds:
    username     : "root"
    instance_type: "db.t2.small"
    version      : "10.6"
    engine_mode  : "serverless"
    encrypt      : True
    backup_retention_period: 32
    parameter_group_family : "aurora-postgresql10"

    snapshot_name    : ""  # Optional, backup recovery
    source_cluster_id: ""  # Optional, creates read replica
```

Example Playbooks
-----------------
Rollout the aws-rds-aurora-postgres files with defaults
```yaml
- hosts: localhost
  connection: local
  gather_facts: False
  vars:
    tag_prefix      : "mcf"
    aws_region      : "eu-west-1"
    owner           : "myself"
    account_name    : "my-dta"
    account_abbr    : "dta"
    environment_type: "test"
    environment_abbr: "tst"
    internal_route53:
      domainname: my.cloud
    aws_vpc_params:
      network               : "10.10.10.0/24"
      public_subnet_weight  : 1
      private_subnet_weight : 3
      database_subnet_weight: 1
    aws_lambda_params:
      lambda_roles:
        - aws-lambda-cfn-secretsprovider
  roles:
    - aws-setup
    - aws-iam
    - aws-vpc
    - env-acl
    - aws-vpc-lambda
    - aws-securitygroups
    - aws-lambda
    - aws-rds-aurora-postgres
```
License
-------
GPLv3

Author Information
------------------
Lotte-Sara Laan <lotte@owl-ict.nl>  
Wouter de Geus <wouter@owl-ict.nl>  
Rob Reus <rreus@mirabeau.nl>  
Theo Beers<tbeers@mirabeau.nl>
